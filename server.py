from flask import Flask
from flask import render_template, request
import extractABCD

app = Flask(__name__)
@app.route("/")
def index():
	return render_template("index.html")

@app.route("/request")
def extract():
	extractABCD.extractRun(request.args.get("month"), request.args.get("media"))
	return render_template("index.html")

@app.route("/submit")
def submitEx():
	response = extractABCD.submit()
	print(response)
	return render_template("index.html")

if __name__ == "__main__":
    app.run()