$(document).ready(function(){
	$("#mediaSelect").formSelect();
	$("#monthSelect").formSelect();

	$("#btn-extract").click(function(e){
		e.preventDefault();

		const month = document.getElementById("monthSelect").value;
		const media = document.getElementById("mediaSelect").value;

		if(month == "disabled"){
			alert("Opção para mês Inválida!");
		}else if(media == "disabled"){
			alert("Opção para mídia Inválida!");
		}else{
			dados=[media,month]

			$.ajax({
				type: "GET",
				url: "/request",
				data: {month : month, media : media},
				contentType: 'application/json;charset=UTF-8',
			});
		};

	});

	$("#btn-submit").click(function(e){
		e.preventDefault();

		$.ajax({
			type: "GET",
			url: "/submit",
		});
	});
});