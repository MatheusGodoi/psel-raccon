# psel-raccon

Projeto do processo seletivo da raccon.

Sobre o projeto:
Utilizei Python 3.7.3 para o desenvolvimento do código, e a tecnologia Flask 1.1.1 para criar um simples servidor
para oferecer o serviço requisitado.

O código em si consiste em apenas um arquivo python, responsável por realizar todas as funções descritas no exercício com apenas uma execução
(ou seja, o código extractABCD realiza a extração dos dados, as 4 "filtragens" e escreve em um json),
porém, todas as funções foram modularizadas e quebradas para permitir a fácil melhoria e adaptação das mesmas.

Para o site em si, utilizei Materialize, JavaScript, JQuery e Ajax para fazer tudo funcionar (além do próprio Flask).

# Execução

Basta rodar o arquivo "server.py" e entrar no link [127.0.0.1:5000](url) para acessar a página. Após isso, basta escolher
a mídia e o mês desejado (dos exercícios B e C) para serem buscados, e apertar o "Executar", isso ira gerar um arquivo "resposta.json"
na pasta base. Para enviar o arquivo gerado resposta.json, é só pressionar o botão "Enviar".

Um outro modo mais simples de execução é rodar o arquivo "extractABCD.py" no terminal, e ele irá fazer as buscas 
com o mês 05, a mídia instagram_cpc e vai submeter os resultados automaticamente, mostrando a resposta do post no terminal.