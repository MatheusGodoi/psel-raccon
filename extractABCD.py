import requests
from operator import itemgetter
import os

#Escrita das respostas no arquivo JSON
def writeToJsonABCD(promotions, likes, sumLikes, errors):
	file = open("resposta.json","w+")

	file.write('{\n  "full_name": \"Matheus Vinicius Gouvea de Godoi\",\n  "email": \"mtgodoi97@gmail.com\",\n  "code_link": \"www.gitlab.com/MatheusGodoi/psel-raccon\",\n')

	file.write('  "response_a": [\n')
	for item in promotions:
		file.write('    ' + "{\"product_id\": \"" + str(item[0]) + "\", \"price_field\": " + str(item[1]) + '},\n')

	#Remoção da última vírgula
	file.seek(file.tell() - 2, os.SEEK_SET)
	file.write("\n")
	file.write('  ],\n')

	file.write('  "response_b": [\n')
	for item in likes:
		file.write('    ' + "{\"post_id\": \"" + str(item[0]) + "\", \"price_field\": " + str(item[1]) + '},\n')
	
	#Remoção da última vírgula
	file.seek(file.tell() - 2, os.SEEK_SET)
	file.write("\n")
	file.write('  ],\n')

	file.write('  "response_c": ' + str(sumLikes) + ',\n')
	file.write('  "response_d": ')

	file.write(str(errors))
	file.write("\n}")
	file.close()

#Substituição das Aspas
def cleanQuotes():
	file = open("resposta.json","r+")
	contents = file.read()

	newFile = contents.replace("\'","\"")
	file.close()
	
	file = open("resposta.json","w+")
	file.write(newFile)
	file.close()

#Request para aquisição dos dados
def getData(URL):
	req = requests.get(url = URL)
	return req.json()

#Criação de uma lista DESORGANIZADA com todos items em promoção
def getPromotions(data):
	promList = []
	for obj in data["posts"]:
		if "promocao" in obj["title"]:
			promList.append((obj["product_id"],obj["price"]))

	return promList

#Criação de uma lista DESORGANIZADA com todos items com mais de 700 likes
def getLikes(midia, data):
	likeList = []
	for obj in data["posts"]:
		if obj["media"] == midia:
			if obj["likes"] > 700:
				likeList.append((obj["post_id"],obj["price"]))

	return likeList

#Função que busca inconsistências nos dados
def getErrors(data):
	priceProduct = {}
	errorList = []
	for item in data["posts"]:
		if item["product_id"] not in priceProduct.keys():
			priceProduct[item["product_id"]] = item["price"]
		elif item["product_id"] in priceProduct.keys():
			if item["price"] != priceProduct[item["product_id"]]:
				errorList.append(item["product_id"])

	errorList.sort()
	return errorList

#Somatório do like de todos posts nas mídias pagas durante o mês específicado
def sumLikes(month, data):
	sum = 0
	paidMedias = {"google_cpc", "facebook_cpc", "instagram_cpc"}
	for obj in data["posts"]:
		if obj["media"] in paidMedias:
			dayObj,monthObj,yearObj = obj["date"].split("/")
			if monthObj == month and yearObj == "2019":
				sum = sum + int(obj["likes"])
	return sum

def sortData(typeData, unsortedData):
	#No caso das promoções, é utilizado duas listas vazias para poder filtrar as duplicatas de product_id, sendo
	#uma das listas (IDlist) responsável por armazenar todo os product_id já vistos e que foram adicionador na lista
	#de resultados "filtered"; e a lista "filtered" utilizada para fazer a ordenação dos items
	if typeData == "promotions":
		IDlist = []
		filtered = []
		for item in unsortedData:
			if item[0] not in IDlist:
					IDlist.append(item[0])
					filtered.append(item)
			else:
				continue

		sortedData = sorted(filtered, key=itemgetter(1,0))

	elif typeData == "likes":
		sortedData = sorted(unsortedData, key=itemgetter(1,0))

	else:
		print("Wrong field for sorting")
		return []

	return sortedData

def extractRun(month, media):
	#Variaveis das url, mes, midia, etc.
	urlABC = "https://us-central1-psel-clt-ti-junho-2019.cloudfunctions.net/psel_2019_get"
	urlD = "https://us-central1-psel-clt-ti-junho-2019.cloudfunctions.net/psel_2019_get_error"

	#Request dos dados
	dataset = getData(urlABC)

	#Filtrando as promoções e likes
	unsortedPromotions = getPromotions(dataset)
	unsortedLikes = getLikes(media, dataset)

	#Organização dos dados para ordem crescente, por PREÇO e depois ID
	sortedPromotions = sortData("promotions", unsortedPromotions)
	sortedLikes = sortData("likes", unsortedLikes)

	#Somatório dos likes no mes desejado
	likeAmount = sumLikes(month,dataset)

	#Extracao dos dados novamente com os erros
	dataset = getData(urlD)

	#Resultado com as inconsistências
	priceErrors = getErrors(dataset)

	#Escrita dos resultados no json
	writeToJsonABCD(sortedPromotions, sortedLikes, likeAmount, priceErrors)

	#Devido ao Python utilizar aspas únicas na escrita dos objetos, as mesmas devem ser substituídas 
	#para ser um documento json válido.
	cleanQuotes()

	return

def submit():
	urlPost = "https://us-central1-psel-clt-ti-junho-2019.cloudfunctions.net/psel_2019_post"

	headers = {
    	"Content-Type": "application/json",
	}

	data = open("resposta.json")

	response = requests.post(url=urlPost, headers=headers, data=data)
	return response.text


if __name__ == "__main__":
	extractRun("05","instagram_cpc")
	response = submit()